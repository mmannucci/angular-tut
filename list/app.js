angular.module('peppers', ['peppers.filters', 'peppers.controllers', 'peppers.services'])
.config(function($routeProvider) {
    $routeProvider.
      when('/', {controller:'PeppersCtrl', templateUrl:'list.html'}).
      when('/show/:pepperId', {controller:'PepperCtrl', templateUrl:'detail.html'}).
      otherwise({redirectTo:'/'});
  });

