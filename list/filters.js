angular.module('peppers.filters', [])
.filter('heatRating', function() {
  return function(shuValue) {
     var rating = Math.ceil(shuValue / 200000);
     var ratingString = "";

     for (var i = 0; i < rating; i++) {
        ratingString += "\u2622";
     }

     return ratingString;
  };
});

