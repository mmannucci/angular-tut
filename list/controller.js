angular.module('peppers.controllers', [])
.controller('PeppersCtrl',['$scope', 'Peppers', function ($scope, Peppers) {
  $scope.predicate = "name";
  $scope.sort = false;

  $scope.peppers = Peppers.query();

  $scope.addPepper = function() {
    $scope.peppers.push({name:$scope.pepperName, shu_min:0, shu_max:0});
    $scope.pepperName = '';
  };
}])
.controller('PepperCtrl',['$scope', '$routeParams', function ($scope, $routeParams) {
    $scope.pepperName = $routeParams.pepperId
}])
;
