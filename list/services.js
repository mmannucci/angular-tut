var pepperServices = angular.module('peppers.services', ['ngResource']);
 
pepperServices.factory('Peppers', ['$resource',
  function($resource){
    return $resource('peppers.json', {}, {
      query: {method:'GET', params:{peppersId:'peppers'}, isArray:true}
    });
  }]);

